import gdata.docs.data
import gdata.docs.client
import gdata.data
from gdata.acl.data import AclScope, AclRole
# from app.models import *
import urllib
from bs4 import BeautifulSoup, element
import cssutils 
from secrets import *

def glogin():
  '''Authenticates with Google Docs'''
  client = gdata.docs.client.DocsClient()
  auth_token = client.ClientLogin(APP_EMAIL, APP_PASS, APP_NAME)
  return client 

def create_folder(folder_name, parent_folder=None):
  client = glogin()
  if parent_folder is None:
    return client.Create(gdata.docs.data.FOLDER_LABEL, folder_name)
  else:
    return client.Create(gdata.docs.data.FOLDER_LABEL, folder_name, folder_or_id=parent_folder)

def create_blogger_folder(blogger_email):
  client = glogin()
  main_folder = create_folder('DocBlogger Posts')
  print 'main_folder'
  print main_folder
  resource_id = main_folder.resource_id.text
  scope = AclScope(value=blogger_email, type='user')
  role = AclRole(value='writer')
  print 'role'
  print role
  acl_entry = gdata.docs.data.Acl(scope=scope, role=role)
  print 'acl_entry'
  print acl_entry
  new_acl=client.Post(acl_entry, main_folder.GetAclFeedLink().href)
  print 'new_acl'
  print new_acl
  return resource_id

def create_newdoc(blogger):
  client = glogin()
  new_doc = client.Create('document', 'New Blog Post', blogger.folder_id)
  return new_doc

def get_items_in_folder(folder_id):
  client = glogin()
  uri = gdata.docs.client.FOLDERS_FEED_TEMPLATE % folder_id
  doclist = client.get_doclist(uri=uri)
  return doclist.entry

def get_file_contents(resource_id):
  client = glogin()
  doc = client.get_doc(resource_id)
  title = doc.title.text
  extra_params={'exportFormat':'html', 'format':'html'}
  uri = doc.content.src + '&' + urllib.urlencode(extra_params)
  document = client.get_file_content(uri)
  return document, title

def make_css_unique(post, document):
  soup = BeautifulSoup(document)
  css = soup.style.contents[0]
  prefix = 'd%s' % post.id
  parser = cssutils.parseString(css)
  for rule in parser:
    rule.selectorText+='.'+prefix
  newcss = parser.cssText

  tree = soup.body.descendants
  for child in tree:
    if type(child) == element.Tag:
      previous_class = child.get('class', None)
      if previous_class:
        child['class'].append(prefix)
      else:
        child['class'] = prefix
  body = unicode(soup.body)
  
  return newcss, body