from django import forms
from models import *
from django.forms import ModelForm, Form, ValidationError
from django.contrib.auth.forms import AuthenticationForm

class SettingsForm(ModelForm):
  class Meta:
    model = Blogger
    exclude= ('gmail','user','folder_id',)

class LinkForm(ModelForm):
  class Meta:
    model = Link
    exclude= ('blogger',)