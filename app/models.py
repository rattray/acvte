from django.db import models
from django.contrib.auth.models import User
from django.template.defaultfilters import slugify

# Create your models here.
class Blogger(models.Model):
  name = models.CharField(max_length=100)
  gmail = models.CharField(max_length=100)
  tagline = models.CharField(max_length=100, default="A writer")
  user = models.OneToOneField(User, unique=True, related_name="blogger")
  folder_id = models.CharField(max_length=100)
  font = models.CharField(max_length=100, default="Lato")
  url = models.CharField(max_length=200, blank=True)

  @property
  def folder_id_short(self):
    return self.folder_id.split(':')[1]

  @property
  def gmail_short(self):
    return self.gmail.split('@')[0]

  def __unicode__(self):
    return self.name

class Link(models.Model):
  name = models.CharField(max_length=100)
  url = models.CharField(max_length=100)
  order = models.IntegerField(blank=True, null=True)
  blogger = models.ForeignKey(Blogger, related_name='links')

  def __unicode__(self):
    return self.name

class Post(models.Model):
  name = models.CharField(max_length=100)
  slug = models.SlugField()
  poster = models.ForeignKey(Blogger, related_name='posts')
  content = models.TextField()
  css = models.TextField()
  created = models.DateTimeField()
  last_updated = models.DateTimeField()
  resource_id = models.CharField(max_length=100)
  
  @property
  def resource_id_short(self):
    return self.resource_id.split(':')[1]
  
  class Meta:
    unique_together = (('poster', 'slug'),)

  def __unicode__(self):
    return self.name