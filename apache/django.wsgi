import os
import sys

path = '/home/ubuntu/acvte'
if path not in sys.path:
    sys.path.append(path)
    sys.path.append('/home/ubuntu/')

os.environ['DJANGO_SETTINGS_MODULE'] = 'settings'

import django.core.handlers.wsgi
application = django.core.handlers.wsgi.WSGIHandler()
