from django.conf.urls.defaults import *
from app import views, api
from django.conf import settings

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    (r'^admin/', include(admin.site.urls)),
    (r'media/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.MEDIA_ROOT}),
    
    # user-facing things
    (r'^$', views.blogger_home), # not index
    (r'^accounts/login/$', views.index),
    (r'^edit/$', views.edit),
    (r'^post/(?P<post_slug>.+)/$', views.post),
    
    # api stuff
    (r'api/(.+)/settings/$', api.settings_edit),
    (r'api/signup/$', api.signup),
    (r'api/publish/(.+)/(.+)/$', api.publish),
    (r'api/update/(.+)/(.+)/$', api.update),
    (r'api/post_content/(.+)/$', api.post_content),
    (r'api/link/(\d+)/$', api.link),
    (r'api/link/new/$', api.link_new),
    (r'api/delete/(.+)/(.+)/$', api.delete),
    (r'api/newdoc/(.+)/$', api.newdoc),
    (r'api/newdoc/(.+)/$', api.newdoc),
    (r'api/logout/$', api.logout),
)

