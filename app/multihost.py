# File: multihost.py

##
# A simple middleware component that lets you use a single Django
# instance to server multiple distinct hosts.
# from http://effbot.org/zone/django-multihost.htm, modified. 
##

from django.conf import settings
from django.utils.cache import patch_vary_headers

class MultiHostMiddleware:

    def process_request(self, request):
        try:
            host = request.get_host()
            if host[-3:] == ":80":
                host = host[:-3] # ignore default port number, if present  
            try:
                request.urlconf = settings.HOST_MIDDLEWARE_URLCONF_MAP[host]
            except:
                print 'serving from dynamic urls'
                print host
                request.urlconf = settings.HOST_MIDDLEWARE_URLCONF_DYNAMIC
        except KeyError:
            pass # use default urlconf (settings.ROOT_URLCONF)

    def process_response(self, request, response):
        if getattr(request, "urlconf", None):
            patch_vary_headers(response, ('Host',))
        return response