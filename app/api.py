from django.shortcuts import render_to_response, get_object_or_404, redirect, render
from django.contrib.auth.decorators import login_required
from django.core.urlresolvers import reverse
from django.template.defaultfilters import slugify
from django.contrib import auth
from django.core.validators import validate_email
from django.http import HttpResponse, HttpResponseRedirect
from django.template import RequestContext
from acvte.app.models import *
import datetime, re
from app import docs, forms

@login_required
def delete(request, gmail_short, resource_id):
  blogger = get_object_or_404(Blogger, gmail="%s@gmail.com" % gmail_short)
  if request.user.blogger == blogger:
    post = get_object_or_404(Post, resource_id=resource_id)
    post.delete()
    return HttpResponseRedirect(reverse('app.views.edit'))

@login_required
def publish(request, gmail_short, resource_id):
  blogger = get_object_or_404(Blogger, gmail="%s@gmail.com" % gmail_short)
  if request.user.blogger == blogger:
    html, title = docs.get_file_contents(resource_id)
    post = Post(
      name = title,
      slug = slugify(title),
      poster = blogger,
      content = 'Processing',
      css = 'Processing',
      created = datetime.datetime.now(),
      last_updated = datetime.datetime.now(),
      resource_id = resource_id,
      )
    post.save()
    css, html = docs.make_css_unique(post, html)
    post.css = css
    post.content = html
    post.save()
    return HttpResponseRedirect(reverse('app.views.edit'))

@login_required
def update(request, gmail_short, resource_id):
  blogger = get_object_or_404(Blogger, gmail="%s@gmail.com" % gmail_short)
  if request.user.blogger == blogger:
    html, title = docs.get_file_contents(resource_id)
    post = Post.objects.get(resource_id=resource_id)
    css, html = docs.make_css_unique(post, html)
    post.name = title
    post.content = html
    post.css = css
    post.last_updated = datetime.datetime.now()
    post.save()

    return HttpResponseRedirect(reverse('app.views.edit'))

@login_required
def link(request, link_id):
  if request.method == 'POST':
    link = get_object_or_404(Link, id=link_id)
    # add security
    form = forms.LinkForm(request.POST, instance=link)
    form.save()
    return HttpResponseRedirect(reverse('app.views.edit'))

@login_required
def link_new(request):
  if request.method == 'POST':
    blogger = request.user.blogger
    # add security
    link = Link(blogger=blogger)
    form = forms.LinkForm(request.POST, instance=link)
    form.save()
    return HttpResponseRedirect(reverse('app.views.edit'))

@login_required
def newdoc(request, gmail_short):
  blogger = get_object_or_404(Blogger, gmail="%s@gmail.com" % gmail_short)
  new_doc = docs.create_newdoc(blogger)
  return HttpResponseRedirect(reverse('app.views.edit'))

def settings_edit(request, gmail_short):
  if request.method == 'POST':
    blogger = get_object_or_404(Blogger, gmail="%s@gmail.com" % gmail_short)
    form = forms.SettingsForm(request.POST, instance=blogger)
    blogger = form.save()
    return HttpResponseRedirect(reverse('app.views.edit'))

def logout(request):
  auth.logout(request)
  return HttpResponseRedirect('/')

def signup(request):
  print 'in signup'
  if request.method == 'POST':
    gmail = request.POST.get('gmail')
    password = request.POST.get('password')
    try:
      blogger = Blogger.objects.get(gmail=gmail)
      print "there's a blogger"
    except:
      print "there is no blogger"
      blogger = None
    if blogger != None:
      user = auth.authenticate(username=gmail, password=password)
      if user is not None:
        print user
        if user.is_active:
          print user
          auth.login(request, user)
        else:
          print 'disabled'
          HttpResponse('account has been disabled')
      else:
        print 'not valid'
        HttpResponse('not valid')
    else:
      try:
        validate_email(gmail)
      except:
        return HttpResponse('Please enter a valid gmail address')
      if not re.match(r'.+@gmail.com', gmail):  
        return HttpResponse('Please enter a valid gmail address')
      if not password:
        return HttpResponse('Please enter a password')
      user = User(username=gmail, email=gmail)
      user.set_password(password)
      user.save()
      user = auth.authenticate(username=gmail, password=password)
      if user is not None:
        if user.is_active:
          auth.login(request, user)
        else:
          print 'inactive'
      else:
        print 'invalid login'
      folder_id = docs.create_blogger_folder(gmail)
      blogger = Blogger(
        gmail=gmail, 
        user=user, 
        name=re.match(r'(.+)@gmail\.com', gmail).group(1),
        folder_id=folder_id,
        )
      blogger.save()
    return HttpResponseRedirect(reverse('app.views.edit'))

def post_content(request, post_id):
  post = get_object_or_404(Post, id=post_id)
  return HttpResponse(post.content)