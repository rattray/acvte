from django.contrib import admin
from django.contrib.admin.sites import AdminSite
from acvte.app.models import *
import datetime

admin.site.register(Blogger)
admin.site.register(Post)
admin.site.register(Link)