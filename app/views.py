# Create your views here.
from django.shortcuts import render_to_response, get_object_or_404, redirect, render
from django.contrib.auth.decorators import login_required
from django.template.defaultfilters import slugify
from django.contrib import auth
from django.db.models import Q
from django.core.validators import validate_email
from django.http import HttpResponse, HttpResponseRedirect
from django.template import RequestContext
from acvte.app.models import *
import datetime, re, urllib, json, urllib2
from app import docs, forms, secrets

def index(request):
  print request.domain, request.subdomain
  bloggers = Blogger.objects.all()
  context = {
    'bloggers':bloggers,
    'request':request,
    'today':datetime.date.today(),
  }
  return render_to_response('index.html', RequestContext(request, context))

def blogger_home(request, gmail_short=None):
  if not gmail_short: 
    # we're on a foreign site
    base_url = request.get_host()
    blogger = get_object_or_404(Blogger, url=base_url)
  else:
    base_url = request.get_host() + '/blog/%s/' % gmail_short
    blogger = get_object_or_404(Blogger, gmail="%s@gmail.com" % gmail_short)
  posts = Post.objects.filter(poster=blogger).order_by('-last_updated')
  links = Link.objects.filter(blogger=blogger).order_by('order')
  variants = ','.join(_getFontVariants(blogger.font))

  context = {
    'blogger':blogger, 
    'posts':posts,
    'links':links,
    'today':datetime.date.today(),
    'request':request,
    'variants':variants,
    'base_url':base_url,
  }
  return render_to_response('blogger_home.html', RequestContext(request, context))

@login_required
def edit(request):
  blogger = get_object_or_404(Blogger, user=request.user)
  form = forms.SettingsForm(instance=blogger)
  folder_id = blogger.folder_id
  feed = docs.get_items_in_folder(folder_id)
  posts = [p for p in Post.objects.filter(poster=blogger)]
  resource_ids = [p.resource_id for p in posts]
  links = [[l, forms.LinkForm(instance=l)] for l in Link.objects.filter(blogger=blogger).order_by('order')]
  new_link_form = forms.LinkForm()
  fonts_url = secrets.FONT_URL 
  req = urllib2.Request(fonts_url)
  fonts_available = urllib2.urlopen(req).read()
  # print fonts_available
  fonts = json.loads(fonts_available)
  fonts = [f.get('family') for f in fonts['items']]
  # fonts += extra_fonts()
  # print fonts
  base_url = '/' if request.get_host() == blogger.url else '/blog/%s/' % blogger.gmail_short
  context = {
    'blogger': blogger,
    'form': form, 
    'feed':feed,
    'posts':posts,
    'links':links,
    'new_link_form':new_link_form,
    'resource_ids':resource_ids,
    'fonts':fonts,
    'base_url':base_url,
  }
  return render_to_response('edit.html', RequestContext(request, context))

def post(request, post_slug, gmail_short=None):
  if not gmail_short: 
    blogger = get_object_or_404(Blogger, url=request.get_host())
  else:
    blogger = get_object_or_404(Blogger, gmail="%s@gmail.com" % gmail_short)
  post = get_object_or_404(Post, Q(poster=blogger, slug=post_slug))
  links = Link.objects.filter(blogger=blogger).order_by('order')
  variants = ','.join(_getFontVariants(blogger.font))
  context = {
    'blogger':blogger,
    'post':post,
    'links':links,
    'request':request,
    'variants':variants,
  }
  return render_to_response('post.html', RequestContext(request, context))

def _getFontVariants(font):
  fonts_url = secrets.FONT_URL
  req = urllib2.Request(fonts_url)
  fonts_available = urllib2.urlopen(req).read()
  # print fonts_available
  fonts = json.loads(fonts_available)
  for f in fonts['items']:
    if f['family'] == font:
      variants = f['variants']
  try:
    variants = [v for v in variants if re.match(r'^\d+$', v)]
  except:
    variants = ''
  # print variants
  return variants

def extra_fonts():
  extra_fonts = u'''
  Georgia, Palatino Linotype, Book Antiqua, Palatino, 
  Times New Roman, Times, Arial, Helvetica, 
  Arial Black, Gadget, Impact, Charcoal, Lucida Sans Unicode, 
  Lucida Grande, Tahoma, Geneva, Trebuchet MS, Verdana, Geneva, 
  Courier New, Courier, monospace, Lucida Console, Monaco
  '''
  extra_fonts.replace('\n', '')
  thelist = extra_fonts.split(', ')
  # print thelist
  return thelist